CREATE TABLE IF NOT EXISTS Task_definition (
  id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(50) NOT NULL,
  description varchar(200) ,

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Task_definition_mirror (
  id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(50) NOT NULL,
  description varchar(200) ,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS Task_definition_shared (
  id bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(50) NOT NULL,
  description varchar(200) ,
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into TASK_DEFINITION (name,description) values ('user 1','description1');
insert into TASK_DEFINITION (name) values ('user 2');
insert into TASK_DEFINITION (id,name) values (3,'user 3');
