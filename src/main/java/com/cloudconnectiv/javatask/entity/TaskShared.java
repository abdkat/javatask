package com.cloudconnectiv.javatask.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Task_definition_shared")
public class TaskShared {

    @Id
    protected Long id;
    @Column(name = "name", nullable = false, length = 50)
    private String name;
    @Column(name = "description", nullable = true, length = 200)
    private String description;

    public TaskShared() {
    }

    public TaskShared(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescrption(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description +
                "'}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TaskShared taskShared = (TaskShared) o;
        return Objects.equals(id, taskShared.id) &&
                Objects.equals(name, taskShared.name) &&
                Objects.equals(description, taskShared.description);
    }
}

