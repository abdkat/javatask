package com.cloudconnectiv.javatask.repository;

import com.cloudconnectiv.javatask.entity.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepo extends CrudRepository<Task, Long> {
}
