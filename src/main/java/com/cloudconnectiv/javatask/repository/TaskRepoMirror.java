package com.cloudconnectiv.javatask.repository;

import com.cloudconnectiv.javatask.entity.TaskMirror;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepoMirror extends CrudRepository<TaskMirror, Long> {
}
