package com.cloudconnectiv.javatask.repository;

import com.cloudconnectiv.javatask.entity.TaskShared;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskSharedRepo extends CrudRepository<TaskShared, Long> {
}
