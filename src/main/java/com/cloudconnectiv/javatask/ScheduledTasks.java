package com.cloudconnectiv.javatask;

import com.cloudconnectiv.javatask.entity.Task;

import com.cloudconnectiv.javatask.entity.TaskMirror;
import com.cloudconnectiv.javatask.repository.TaskRepo;
import com.cloudconnectiv.javatask.repository.TaskRepoMirror;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ScheduledTasks {

    @Autowired
    private TaskRepo taskRepo;
    @Autowired
    private TaskRepoMirror taskRepoMirror;
    @Autowired
    private TaskSyncService taskSyncService;

    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    @Scheduled(fixedRate = 2000)
    private void execute() {
        // Synchronize tables then print them
        taskSyncService.sync();
        printTable();
        printMirrorTable();
    }
    // Helper function to Print tables 1 in the database
    private void printTable() {
        List<Task> tasks = new ArrayList<>();
        taskRepo.findAll().forEach(tasks::add);
        logger.info("\n==========================================");
        logger.info("<<<<<<<< Contents of task_definition table: >>>>>>>>>");
        tasks.forEach(s -> logger.info("{}", s.toString()));
    }
    // Helper function to Print tables 2 in the database
    private void printMirrorTable() {
        List<TaskMirror> tasks_mirror = new ArrayList<>();
        taskRepoMirror.findAll().forEach(tasks_mirror::add);
        logger.info("\n==========================================");
        logger.info("<<<<< Contents of task_definition_mirror table: >>>>>>");
        tasks_mirror.forEach(s -> logger.info("{}", s.toString()));
    }
}
