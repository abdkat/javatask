package com.cloudconnectiv.javatask;

import com.cloudconnectiv.javatask.entity.Task;
import com.cloudconnectiv.javatask.repository.TaskRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value = "/tasks")
public class TaskController {
    @Autowired
    private TaskRepo taskRepo;
    @RequestMapping( method = RequestMethod.GET)
    public Iterable<Task> findAllTasks() {
        return taskRepo.findAll();
    }

    @RequestMapping( value = "/{id}" ,method = RequestMethod.GET)
    public ResponseEntity<?> findOneTask(@PathVariable Long id) {
        Task found = taskRepo.findById(id).orElse(null);
        if(found == null) {
            return new ResponseEntity<>("Not found ", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(found, HttpStatus.OK);
    }

    @RequestMapping( method = RequestMethod.POST)
    public ResponseEntity<?> insertTask(@RequestBody Task task) {
        List<Task> tasks = new ArrayList<>();
        taskRepo.findAll().forEach(tasks::add);
        if(!tasks.contains(task)) {
            taskRepo.save(task);
            return new ResponseEntity<>(task.getName()+" inserted", HttpStatus.OK);
        }
        return new ResponseEntity<>(task.getName()+" already exist !", HttpStatus.CONFLICT);
    }

    @RequestMapping( method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTask(@RequestBody Task task) {
        Task found = taskRepo.findById(task.getId()).orElse(null);
        if (found == null) {
            return new ResponseEntity<>(task.getName()+" is not in the database !", HttpStatus.NOT_FOUND);
        }else if(found.equals(task)){
            taskRepo.delete(task);
            return new ResponseEntity<>(task.getName()+" deleted ", HttpStatus.OK);
        } else {
            return new ResponseEntity<>(task.getName() + " has different info in the database ", HttpStatus.OK);
        }
    }

    @RequestMapping( method = RequestMethod.PUT)
    public ResponseEntity<?> updateTask(@RequestBody Task task) {
        Task found = taskRepo.findById(task.getId()).orElse(null);
        if (found == null) {
            return new ResponseEntity<>(task.getName()+" is not in the database !", HttpStatus.NOT_FOUND);
        }else if(found.equals(task)){
            return new ResponseEntity<>(task.getName()+" already exist !", HttpStatus.CONFLICT);
        } else {
            taskRepo.save(task);
            return new ResponseEntity<>(task.getName() + " updated successfully ", HttpStatus.OK);
        }
    }

}
