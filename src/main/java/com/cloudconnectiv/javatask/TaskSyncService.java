package com.cloudconnectiv.javatask;

import com.cloudconnectiv.javatask.entity.Task;
import com.cloudconnectiv.javatask.entity.TaskMirror;
import com.cloudconnectiv.javatask.entity.TaskShared;
import com.cloudconnectiv.javatask.repository.TaskRepo;
import com.cloudconnectiv.javatask.repository.TaskRepoMirror;
import com.cloudconnectiv.javatask.repository.TaskSharedRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class TaskSyncService {
    @Autowired
    private TaskRepo taskRepo;
    @Autowired
    private TaskRepoMirror taskRepoMirror;
    @Autowired
    private TaskSharedRepo taskSharedRepo;

     void sync() {

         // Get all tables
         List<Task> taskTable = new ArrayList<>();
         taskRepo.findAll().forEach(taskTable::add);
         List<TaskMirror> taskMirrorsTable = new ArrayList<>();
         taskRepoMirror.findAll().forEach(taskMirrorsTable::add);
         List<TaskShared> taskSharedTable = new ArrayList<>();
         taskSharedRepo.findAll().forEach(taskSharedTable::add);

         // Check if shared contains more entries than others, then delete them from the original and mirror tables
         for (TaskShared taskShared : taskSharedTable) {
             Task task = new Task(taskShared.getId(), taskShared.getName(), taskShared.getDescription());
             TaskMirror taskMirror = new TaskMirror(taskShared.getId(), taskShared.getName(), taskShared.getDescription());
             // In the shared but not the original, then delete them from the mirror and the shared
             if (!taskTable.contains(task)) {
                 taskRepoMirror.delete(taskMirror);
                 taskSharedRepo.delete(taskShared);
             }
             // In the shared but not the mirror, then delete them from the original and the shared
             if (!taskMirrorsTable.contains(taskMirror)) {
                 taskRepo.delete(task);
                 taskSharedRepo.delete(taskShared);
             }
         }
        // For all entries in the original but not in the shared,  then insert them the mirror and shared
         for (Task task: taskTable ) {
             TaskShared taskShared = new TaskShared(task.getId(), task.getName(), task.getDescription());
             if (!taskSharedTable.contains(taskShared)) {
                 taskRepoMirror.save(new TaskMirror(task.getId(), task.getName(), task.getDescription()));
                 taskSharedRepo.save(new TaskShared(task.getId(), task.getName(), task.getDescription()));
             }
         }
         // For all entries in the mirror but not in the shared, then insert them the original and shared
         for (TaskMirror taskMirror: taskMirrorsTable) {
             TaskShared taskShared = new TaskShared(taskMirror.getId(), taskMirror.getName(), taskMirror.getDescription());
             if (!taskSharedTable.contains(taskShared)) {
                 taskRepo.save(new Task(taskMirror.getId(), taskMirror.getName(), taskMirror.getDescription()));
                 taskSharedRepo.save(new TaskShared(taskMirror.getId(), taskMirror.getName(), taskMirror.getDescription()));
             }
         }
     }
}
