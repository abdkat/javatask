package com.cloudconnectiv.javatask;


import com.cloudconnectiv.javatask.entity.Task;
import com.cloudconnectiv.javatask.entity.TaskMirror;
import com.cloudconnectiv.javatask.repository.TaskRepo;
import com.cloudconnectiv.javatask.repository.TaskRepoMirror;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskSyncServiceTest {

    @TestConfiguration
    static class TaskSyncServiceTestContextConfiguration {

        @Bean
        public TaskSyncService taskSyncService() {
            return new TaskSyncService();
        }
    }
    @Autowired
    TaskRepo taskRepo;
    @Autowired
    TaskRepoMirror taskRepoMirror;
    @Autowired
    TaskSyncService taskSyncService;

    @Test
    public void syncTest(){
        Task abdullah = new Task(3L, "Abdullah", "New Java dev");
        TaskMirror hamza = new TaskMirror(4L, "Hamza", "Java dev");
        Task mohammad = new Task(5L, "Mohammad", "Senior Java dev");

        // Insert Abdullah into table 1 and synchronize
        taskRepo.save(abdullah);
        taskSyncService.sync();

        // Insert Hamza into table 2 and synchronize
        taskRepoMirror.save(hamza);
        taskSyncService.sync();

        // Insert Mohammad into table 2 and synchronize
        taskRepo.save(mohammad);
        taskSyncService.sync();

        // Delete hamza from table 1 and abdullah from table 2 respectively
        taskRepo.delete(new Task(hamza.getId(), hamza.getName(), hamza.getDescription()));
        taskRepoMirror.delete(new TaskMirror(abdullah.getId(), abdullah.getName(), abdullah.getDescription()));
        taskSyncService.sync();

        // retrieve both original and mirror tables and compare its contents
        List<Task> taskTable = new ArrayList<>();
        taskRepo.findAll().forEach(taskTable::add);
        List<TaskMirror> mirrorTable = new ArrayList<>();
        taskRepoMirror.findAll().forEach(mirrorTable::add);

        for (int i=0; i<taskTable.size(); i++){
            Assert.assertEquals(taskTable.get(i).getId(), mirrorTable.get(i).getId());
            Assert.assertEquals(taskTable.get(i).getName(), mirrorTable.get(i).getName());
            Assert.assertEquals(taskTable.get(i).getDescription(), mirrorTable.get(i).getDescription());
        }

        Logger.getLogger("Task table").warning(taskTable.toString());
        Logger.getLogger("Mirror table").warning(mirrorTable.toString());
    }


}